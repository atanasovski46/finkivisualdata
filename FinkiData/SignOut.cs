//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class SignOut
    {
        public int ID { get; set; }
        public string DocumentNo { get; set; }
        public System.DateTime Date { get; set; }
        public int StudentID { get; set; }
        public string Description { get; set; }
    }
}
