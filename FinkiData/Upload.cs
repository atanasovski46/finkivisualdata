//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Upload
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string FullPath { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public string RoothFolder { get; set; }
        public Nullable<int> FacultyId { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> Message { get; set; }
    }
}
