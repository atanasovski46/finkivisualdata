//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Faculty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Logo { get; set; }
        public string Url { get; set; }
        public string ZiroSmetka { get; set; }
        public string CelNaDoznaka { get; set; }
        public string NazivNaPrimac { get; set; }
        public string BankaNaPrimac { get; set; }
        public string UplatnaSmetka { get; set; }
        public string PrihodnaSifra { get; set; }
        public string SmetkaNaBudKor { get; set; }
        public string ShortName { get; set; }
        public string ShortIndeks { get; set; }
        public Nullable<bool> IsUsingTax { get; set; }
        public string TaxUserName { get; set; }
        public string TaxPassword { get; set; }
        public Nullable<decimal> CoursesAgainPercent { get; set; }
        public Nullable<bool> IsUsingMoodle { get; set; }
        public string MoodleString { get; set; }
        public string MoodleTable { get; set; }
        public string MoodleTableInsert { get; set; }
        public Nullable<int> RankingTypeId { get; set; }
        public string FzomPlaceCode { get; set; }
        public Nullable<bool> IsUsingTickets { get; set; }
    }
}
