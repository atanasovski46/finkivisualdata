//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class TermQuotaNationality
    {
        public int Id { get; set; }
        public Nullable<int> TermId { get; set; }
        public Nullable<int> NationalityId { get; set; }
        public Nullable<int> Number { get; set; }
    }
}
