﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace FinkiData.Controllers
{

    public class ValuesController : ApiController
    {
        
        [Route("api/AllStudentDataJson")]
        [HttpGet]
        public HttpResponseMessage AllStudentDataJson()
        {

            UniDataFinki data = new UniDataFinki();
            var FinkiStudentsSimple = (data.Students.Where(st => st.FacultyId == 1)
             .Join(data.Programmes, (st => st.StartProgrammeID), (prog => prog.ID), (st, prog) => new { st, prog })
             .Join(data.Programmes, (std => std.st.LastProgrammeId), (prog => prog.ID), (st, prog) => new { st, prog })
             .Join(data.StudentStatus, (std => std.st.st.StudentStatusID), (stdStatus => stdStatus.ID), (std, stdStatus) => new { std, stdStatus })
             .Join(data.Nationalities, (st => st.std.st.st.NationalityID), (stdNationality => stdNationality.ID), (st, stdNationality) => new { st, stdNationality })
             //  .Join(data.StudentsSemesters, (std => std.std.st.st.ID), (stdSemester => stdSemester.StudentID), (std, stdSemester) => new { std, stdSemester })
             .Join(data.Quotas, (st => st.st.std.st.st.LastQuotaPriceId), (stdQuota => stdQuota.ID), (st, stdQuota) => new { st, stdQuota })
             .Select(g =>
                    new
                    {
                        Index = g.st.st.std.st.st.ID,
                        Pol = g.st.st.std.st.st.Sex,
                        StartProgId = g.st.st.std.st.st.StartProgrammeID,
                        StartProgCode = g.st.st.std.st.prog.Code,
                        StartProgName = g.st.st.std.st.prog.Name,
                        LastProgId = g.st.st.std.st.st.LastProgrammeId,
                        LastProgCode = g.st.st.std.st.prog.Code,
                        LastProgName = g.st.st.std.st.prog.Name,
                        StudentStatusID = g.st.st.std.st.st.StudentStatusID,
                        StudentStatus = g.st.st.stdStatus.Status,
                        NationalityID = g.st.stdNationality.ID,
                        Nationality = g.st.stdNationality.Nationality1,
                        SumCredits = g.st.st.std.st.st.SumCredits,
                        g.st.st.std.st.st.SumAverage,
                        g.st.st.std.st.st.StudyCycleID,
                        BirthDate = g.st.st.std.st.st.BirthDate,
                        LastStdQuotaID = g.stdQuota.ID,
                        LastStdQuotaName = g.stdQuota.QuotaName
                    })
             );
            //.ToList();
            var studentData = (data.Students.Where(st => st.FacultyId == 1)
                 .Join(data.StudentsSemesters, (stdSem => stdSem.ID), (stdSemester => stdSemester.StudentID), (std, stdSemester) => new { std, stdSemester })
                 .Join(data.Semesters, (std => std.stdSemester.SemesterID), (sem => sem.ID), (std, stdSemester) => new { std, stdSemester })
                 .GroupBy(std => std.std.std.ID)
                 .Select(g =>
                    new
                    {
                        StudentID = g.Key,
                        StartYearSemester = g.Min(s => s.stdSemester.StartDate),
                        LastYear = g.Max(s => s.stdSemester.StartDate),
                        LastSemesterDate = g.Max(s => s.stdSemester.EndDate),
                        NumSemesters = g.Count(),
                        AmmountPayed = g.Sum(s => s.std.stdSemester.AmountPayed),
                        AmmountToPay = g.Sum(s => s.std.stdSemester.AmountToPay),

                    })
                    )
                    .Join(FinkiStudentsSimple, (stEndSem => stEndSem.StudentID), (stData => stData.Index), (stEndSem, stData) => new { stEndSem, stData })
                    .Join(data.StudyCycles, (stdData =>  stdData.stData.StudyCycleID) , (stdCycle => stdCycle.ID ), (stdData, stdCycle) => new
                    {

                        //   Index = stEndSem.StudentID,
                        Sex = stdData.stData.Pol,
                        StartProgId = stdData.stData.StartProgId,
                        StartProgCode = stdData.stData.StartProgCode,
                        StartProgName = stdData.stData.StartProgName,
                        LastProgId = stdData.stData.LastProgId,
                        LastProgCode = stdData.stData.LastProgCode,
                        LastProgName = stdData.stData.LastProgName,
                        StudentStatusID = stdData.stData.StudentStatusID,
                        StudentStatus = stdData.stData.StudentStatus,
                        //  StudentID = stEndSem.StudentID,
                        NationalityID = stdData.stData.NationalityID,
                        Nationality = stdData.stData.Nationality,
                        StartYearSemester = stdData.stEndSem.StartYearSemester,
                        LastYear = stdData.stEndSem.LastYear.Year,
                        BirthDate = stdData.stData.BirthDate.Year + "-" + stdData.stData.BirthDate.Month,
                        stdData.stData.LastStdQuotaID,
                        stdData.stData.LastStdQuotaName,
                        StudyCycleName = stdCycle.Name,
                        stdData.stData.SumCredits,
                        stdData.stData.SumAverage,
                        stdData.stData.StudyCycleID,
                        stdData.stEndSem.LastSemesterDate,
                        stdData.stEndSem.NumSemesters,
                        stdData.stEndSem.AmmountPayed,
                        stdData.stEndSem.AmmountToPay


                    })

                    .ToList();


            return Request.CreateResponse(HttpStatusCode.OK, studentData);
        }


        [Route("api/ProsekPoPredmetiJSON")]
        [HttpGet]
        public HttpResponseMessage ProsekPoPredmetiJSON()
        {
            UniDataFinki data = new UniDataFinki();
            var ProsekDataList = (data.Students.Where(st => st.FacultyId == 1)
             .Join(data.ExamsPasseds, (std => std.ID), (exmPassed => exmPassed.StudentId), (std, exmPassed) => new { std, exmPassed })
                .Join(data.Courses, (StdExmPassed => StdExmPassed.exmPassed.CourseId), (courses => courses.ID), (StdExmPassed, course) => new { StdExmPassed, course })
                .Where(allData => allData.StdExmPassed.std.FacultyId == 1 && allData.StdExmPassed.exmPassed.Grade != 5 && allData.StdExmPassed.exmPassed.Grade != 11)

              .Select(g => new
              {
               //   g.StdExmPassed.std.Indeks,
                  g.StdExmPassed.exmPassed.Grade,
                  g.course.Name,
                  g.course.ID,
                  g.StdExmPassed.exmPassed.ExamDate


              })
                ).ToList();


            return Request.CreateResponse(HttpStatusCode.OK, ProsekDataList);
        }


    }
}
