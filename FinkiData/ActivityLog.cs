//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityLog
    {
        public System.Guid ID { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public System.Guid UserId { get; set; }
        public string Activity { get; set; }
        public string PageURL { get; set; }
    }
}
