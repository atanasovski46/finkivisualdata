﻿//Custom colors
var color20 = ["#A07A19", "#AC30C0", "#EB9A72", "#BA86F5", "#EA22A8"];
var color30 = ["#b39fbe", "#42be29", "#7033f6", "#f18d35", "#c3055e", "#486c3b", "#6554b6", "#03b4de", "#9b4c3e", "#39b99b", "#e383e0", "#b5a659", "#a422b7", "#4c6779", "#8b4f7a", "#fa7f9f", "#a0a89d", "#6db757", "#88591b", "#cb9c7c", "#7b5b5b", "#9caf2e", "#5a5e97", "#237214", "#b53620", "#8ab07b", "#993e98", "#6c47d6", "#ed8e5c", "#d391be", "#5bb3bd", "#d79b33", "#28be56", "#bc107b", "#6f5c79", "#5e655b", "#9fa0df", "#216897", "#6e6418", "#77b72b", "#95b058", "#71abde", "#954d5c", "#68643c", "#e78f7d", "#835a3d", "#5bb87a", "#78b19c", "#366d5a", "#7d5198", "#df909e", "#2f61b6", "#d29b5a", "#b23740", "#ada77c", "#8ea9bd", "#bba630", "#c19d9d", "#f081bf", "#c393df", "#9f4b1d", "#516c16", "#b21999", "#902bd6", "#a53b7a", "#ac395d", "#3458d6", "#8742b7", "#324cf6"];
var color40 = ["#96caff", "#faff16", "#fda899", "#13fcbb", "#f7fef7", "#eba6ff", "#bcca83", "#57e502", "#feaf23", "#c6c0cb", "#38f1fd", "#9ccfbe", "#f9feae", "#a6fe8e", "#bfcd03", "#ebd3b6", "#fec0df", "#c2eefc", "#cac2fc", "#bcfcd6", "#f4e5ff", "#f9dc56", "#80da93", "#ebb678", "#b1fb27", "#30dfc8", "#8dd0df", "#1ff479", "#cedbd8", "#d2e6b8", "#85feea", "#97db5c", "#deb7b7", "#cad5ef", "#d0eb6d", "#fe9fe1", "#bdc7ad", "#fcdadf", "#d9bae2", "#dac05a", "#ecda91", "#fef5d0", "#ffa4bc", "#43d9fd", "#d2fcf5", "#b1c7d0", "#e6f0fb", "#83e5bd", "#b2e598", "#fdfb73", "#73fd58", "#a6cfa2", "#a1e3e1", "#e4e21c", "#8ef8b0", "#e2c30e", "#fdc7ff", "#cefeb9", "#0be597", "#d4c491", "#fdc5b5", "#e4fdde", "#d5ccc7", "#acdaf8", "#e4c9da", "#fdc664", "#badfc5", "#bbd15f", "#fdf1f7", "#b4c3ed", "#93e8fd", "#ddd9e3", "#e5e5d7", "#98e116", "#e0d0fb", "#fccd95", "#aafdfe", "#5cd9de", "#deba9d", "#dce994", "#d6b2fb", "#c5bedd", "#bec4c3", "#feab79", "#82ea7f", "#fed7f3", "#8affd2", "#d6fc50", "#e8aed9", "#f7b7c2", "#2bf4e5", "#ffad53", "#11ff13", "#d3fe94", "#c2e530", "#aef161", "#fee5d4", "#a3d378", "#dad351", "#fec328", "#a4e3b3", "#a9efd9", "#feeb8a", "#85d4cd", "#e0b4c9", "#55e358", "#bddbe1", "#48ffa0", "#d6eee1", "#fee3b0", "#ff99fd", "#3fe0b0", "#84d7fe", "#7be9d3", "#1bf3cd", "#d4d4af", "#d6d278", "#70fffe", "#aac8e1", "#ffb0fe", "#b5ccbf", "#c6ccd5", "#ccd9c5", "#feee3e", "#cdc0ab", "#58e182", "#e9b8f8", "#f7bc97", "#b4e275", "#bee8e1", "#e1e3ff", "#ebebb2", "#8fd4af", "#c0d3a2", "#e7c67d", "#6ceba9", "#d8cde4", "#eacdc6", "#d4eef1", "#e7e8e9", "#7deaec", "#88f31d", "#c0efbe", "#75d8be", "#a0d1d3", "#fdacd4", "#09f64f", "#c4dc8c", "#d0d1fc", "#ecccf1", "#f7d517", "#d0dde8", "#e7dadc", "#78fe8b", "#e6e373", "#cde5fd", "#b1fead", "#e7fec5", "#e9fdfd", "#b9c3d9", "#cebec1", "#eab3a4", "#edb84a", "#8aea52", "#bad5fd", "#a7ddea", "#f9c9d5", "#f2deec", "#fbf4e7", "#b3d8cf", "#feb9ed", "#96e89d", "#d5d99c", "#efd16f", "#ecfb92", "#d0bbd2", "#89d97b", "#cdc47c", "#acd345", "#21e0f4", "#9ed693", "#9adfcd", "#ccdc4c", "#e3e3c2", "#d8f003", "#e7f158", "#b8fff2", "#cdb9e9", "#faa8ae", "#cac75c", "#d7d119", "#eccb48", "#bbd7ea", "#a0f3c1", "#cbeaa6", "#c5f4e0", "#ede9f5", "#b8bffd", "#97cced", "#70dba6", "#72dbf2", "#e6bddd", "#ddc2fe", "#b8ddb0", "#e7d29e", "#c7ec8c", "#d1edce", "#c8fb75", "#f9ffe8", "#e9abeb", "#b5d1b3", "#feb6af", "#ffba6e", "#92f772", "#e8f4cb", "#ffeda8", "#e5ff77", "#f6f8fe", "#c4c699", "#c5ca3f", "#e9bb29", "#19e2dc", "#d9bfb4", "#c6cabf", "#e6c091", "#bcd0d0", "#d5cbd3", "#aae346", "#d0d2d3", "#f1c7ab", "#72ef95", "#cdda6f", "#d9d5c0", "#a2eb81", "#72f3cc", "#91efe6", "#fed284", "#a6edf6", "#4bfee0", "#9bfb4f", "#c1f3f6", "#e0f9ab", "#d4ffd2", "#fffac1", "#a9c5fd", "#f0acc4", "#7adf34", "#a8d516", "#86dade", "#6cdfd7", "#a7d5b9", "#b4d899", "#f1c0bf", "#a9e8c7", "#e6e04c", "#fcd4bf", "#67feb6", "#ddfeee", "#f5af92", "#a7cddc", "#c7cae9", "#e1c2c9", "#06f6a4", "#64f36c", "#ffd056", "#e9d3ea", "#94f595", "#eadbcc", "#acf2b2", "#a2fde0", "#f5e5c6", "#f7eb67", "#f1e9e6", "#e2efdb", "#fbfd52", "#aacac8", "#6fdf6e", "#23e83d", "#d6c33e", "#edbb66", "#96d8f1", "#d9c9a7", "#30ef8c", "#66ed41", "#eabeed", "#c3dbce", "#d6d6e9", "#f8c6ee", "#d5d9d1", "#96f0cc", "#c7ef56", "#8ef6ff", "#aff0e8", "#b7f593", "#dee7e0", "#bdff5c", "#eff01a", "#d7f0fe", "#bdfdc3", "#f0ef9c", "#d3fe14", "#bec0ea", "#c6c2be", "#eab1b3"];
var color50 = ["#32212F", "#3A3143", "#3D4256", "#3C5468", "#366778", "#2F7A82", "#2E8E88", "#3AA189", "#52B386", "#72C57F", "#98D477", "#C1E26F", "#EFEE6B"];
var color60 = ['#a50026', '#d73027', '#f46d43', '#fdae61', '#fee090', '#ffffbf', '#e0f3f8', '#abd9e9', '#74add1', '#4575b4', '#313695'];
var color70 = ["#eabd5d", " #cb5b5a", "#ac557a", "#8d4c7d", "#6b406e", "#40324f", "#9f5dea", "#cb5abd", "#ac5562", "#8d594c", "#6e5740", "#6e5740", "#4f4632"];
var color = color30.sort();

//Global variables
var subjectData = [];
var myData = [];

var crossfilteredData = [];
var programmes = {};
var topTypes;
var dimensions = {};
var groups = {};
var charts = {};

var chartWidth = $("#programmeChartDiv").width();
var chartHeight = 200;
var margins = { top: 10, right: 50, bottom: 35, left: 45 };
var innerRadius = 40;
var singleCircle = 30;

var numberFormat1 = d3.format(",.0f");
var numberFormat2 = d3.format(",.2f");

function percentFormat(v) {
    return v + '%';
}

var gender = {
    0: 'Женски',
    1: 'Машки'
};

var gradeList = {
    0: "Без просек",
    6: "6.00 - 6.49",
    7: "6.50 - 7.49",
    8: "7.50 - 8.49",
    9: "8.50 - 9.49",
    10: "9.50 - 9.99",
    11: "10.00"
};


var developmentMode = false;


$(document).ready(function () {
    if (!developmentMode) {
        console.log = function () {
        }
    }

    //Sticky Navbar
    //window.onscroll = function () { myFunction() };

    //var header = document.getElementById("nav-header");

    //var sticky = header.offsetTop;

    //function myFunction() {
    //    if (window.pageYOffset >= sticky + 5) {
    //        header.classList.add("sticky");
    //    } else {
    //        header.classList.remove("sticky");
    //    }
    //}


    initCharts();

    function initCharts() {
        //Load Student Data
        loadStudentsData();
        //Load Subjects Data
        loadSubjects();


        function loadStudentsData() {
            d3.json("/api/AllStudentDataJson", (function (error, data) {

            }, function (error, rows) {

                console.log(rows);

                var tempYearLastSem = "";
                var rowsLng = rows.length;
                rows.forEach(function (d) {
                    tempYearLastSem = "";
                    d.StartYear = d.StartYearSemester.substring(0, 4);
                    if (parseInt((d.LastSemesterDate.substring(5, 7))) > 6) {
                        d.SemesterType = 0;
                        d.SemesterTypeName = "Зимски ";
                        tempYearLastSem = "Зимски " + d.LastSemesterDate.substring(0, 4);
                    } else {
                        d.SemesterType = 1;
                        d.SemesterTypeName = "Летен ";
                        tempYearLastSem = "Летен " + d.LastSemesterDate.substring(0, 4);
                    }
                    d.YearLastSem = tempYearLastSem;

                    //BIRTH DATE CALCULATIONS
                    var str = d.BirthDate.split('-');
                    var firstdate = new Date(str[0], str[1], "00");
                    var today = new Date();
                    var dayDiff = Math.ceil(today.getTime() - firstdate.getTime()) / (1000 * 60 * 60 * 24 * 365);
                    d.Age = parseInt(dayDiff);
                    if (d.StudyCycle == 0) { d.AgeFirstCycle = parseInt(dayDiff); }
                });

                myData = rows;

                loadCharts();
            }))
        }


        function loadSubjects() {
            d3.json("/api/ProsekPoPredmetiJSON", (function (error, data) {
            }, function (error, rows) {

                rows.forEach(function (d) {

                });
                subjectData = rows;
                console.log(subjectData);

                loadSubjectsCharts();
            }))
        }

        function loadSubjectsCharts() {

            crossfilterDataSubjects = crossfilter(subjectData);
            generateDimensionsSubjects();
            generateGroupsSubjects();

            function generateDimensionsSubjects() {
                dimensions["Grade"] = crossfilterDataSubjects.dimension(function (d) {
                    return d.Grade;
                });

                dimensions["ExamYear"] = crossfilterDataSubjects.dimension(function (d) {
                    if (("Undefined" != (d.ExamDate)) && ("Null" != (d.ExamDate)) && (null != (d.ExamDate))) {
                        return parseInt(d.ExamDate);
                    } else return null;
                });

                dimensions["Name"] = crossfilterDataSubjects.dimension(function (d) {
                    return d.Name;
                });
            }

            function generateGroupsSubjects() {
                groups["Name"] = dimensions["Name"].group();
                groups["Grade"] = dimensions["Grade"].group();
                groups["Subjects"] = dimensions["Name"].group().reduce(reduceAdd1, reduceRemove1, reduceInitial1);
                groups["ExamYear"] = dimensions["ExamYear"].group();

                function reduceInitial1() {
                    return { count: 0, total: 0, something: 0, prosek: 0 };
                }
                function reduceAdd1(p, v) {
                    if (v.Grade != null && v.Grade != 0) {
                        ++p.count;
                        p.total += v.Grade;
                        if (p.count > 1000) {
                            p.something = p.count;
                        }
                        p.prosek = p.total;
                        return p;
                    }
                    return p;
                }

                function reduceRemove1(p, v) {
                    if (v.Grade != null && v.Grade != 0) {
                        --p.count;
                        p.total -= v.Grade;
                        if (p.count > 1000) {
                            p.something = p.count;
                        }
                        if ((p.total / p.count) > 9.8) {
                            p.prosek = p.total;
                        }
                    }
                    return p;
                }
            }

            function remove_empty(source_group) {
                return {
                    all: function () {
                        return source_group.all().filter(function (d) {
                            if (d.value.count > 0)
                                return (d.value.prosek / d.value.count) > 6;
                        });
                    }
                };
            }

            function remove_empty_bins(source_group) {
                return {
                    all: function () {
                        return source_group.all().filter(function (d) {
                            return d.value > 0;
                        });
                    }
                };
            }

            nonEmptyHist = remove_empty_bins(groups["Name"])
            charts['chart-hist-spend'] = dc.rowChart("#chart-hist-spend")
            charts['chart-hist-spend']
                .width(chartWidth).height(chartHeight + 100)
                .dimension(dimensions["Name"])
                .group(nonEmptyHist)
                .cap(20)
                .othersGrouper(null)
                .x(d3.scaleBand().domain(groups["Name"]))
                //      .xUnits(dc.units.ordinal)
                .elasticX(true)
                //     .elasticY(true)
                .ordering(function (d) {
                    return -d.value;
                })

            nonEmptyHist = remove_empty(groups["Subjects"])
            charts['subjectGradeChart'] = dc.rowChart("#subjectGradeChart")
                .width(chartWidth + 190).height(chartHeight + 80)
                .dimension(dimensions["Name"])
                .group(nonEmptyHist)
                .x(d3.scaleBand().domain(groups["Name"]))
                .cap(15)
                .othersGrouper(null)
                .elasticX(true)
                //   .elasticY(true)
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .ordering(function (d) {
                    return d.value;
                })
                .valueAccessor(function (p) { return p.value.count > 0 ? p.value.total / p.value.count : 0; });

            charts['ExamYearChart'] = dc.barChart("#ExamYearChart")
                .width($("#ExamYearChart").width() + 30).height(chartHeight + 80)
                .dimension(dimensions["ExamYear"])
                .group(groups['ExamYear'])
                .margins(margins)
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .x(d3.scaleBand().domain(groups["ExamYear"]))
                .xUnits(dc.units.ordinal)
                .centerBar(false)
                .elasticY(true)
                .elasticX(true)
            dc.renderAll();
            $('.modal-loading-spinner').addClass('remove-loader');
        }

        function loadCharts() {
            crossfilterData();
            generateDimensions();
            generateGroups();

            function crossfilterData() {
                console.log('Start: Crossfiltering data');
                var t = new Date();
                crossfilteredData = crossfilter(myData);
                console.log('End: Crossfiltering data (' + (new Date().getTime() - t.getTime()) + ' ms)');
            }

            function generateDimensions() {

                //Age Dimension
                dimensions["StudentAge"] = crossfilteredData.dimension(function (d) {
                    return d.Age;
                });
                //Quota Dimension
                dimensions["LastStudentQuota"] = crossfilteredData.dimension(function (d) {
                    return d.LastStdQuotaName;
                });

                console.log('Start: Generating dimensions');
                var t = new Date();


                dimensions["StartYear"] = crossfilteredData.dimension(function (d) {
                    return d.StartYear;
                });

                dimensions["Sex"] = crossfilteredData.dimension(function (d) {
                    // return d.Sex;
                    if (d.Sex) {
                        return 1;
                    } else {
                        return 0;
                    }
                });

                dimensions["SumCredits"] = crossfilteredData.dimension(function (d) {
                    return d.SumCredits;
                });

                dimensions["SumAverage"] = crossfilteredData.dimension(function (d) {
                    var res = 0;
                    if (d.SumAverage == 10) {
                        res = 11;
                    }
                    else {
                        if (d.SumAverage % 1 <= 0.5)
                            res = Math.ceil(d.SumAverage);
                        else
                            res = Math.floor(d.SumAverage);
                    }
                    return res;
                });
                dimensions["LastSemester"] = crossfilteredData.dimension(function (d) {
                    return d.YearLastSem;
                });

                dimensions["SemesterType"] = crossfilteredData.dimension(function (d) {

                    return d.SemesterTypeName;
                });

                dimensions["TotalSemesters"] = crossfilteredData.dimension(function (d) {
                    return d.NumSemesters;
                });

                dimensions["StudentStatus"] = crossfilteredData.dimension(function (d) {

                    return d.StudentStatus;
                });

                dimensions["Programme"] = crossfilteredData.dimension(function (d) {
                    return [d.LastProgCode, d.StartProgName];
                });

                dimensions["StudyCycle"] = crossfilteredData.dimension(function (d) {
                    return d.StudyCycleName;
                });

                dimensions["Count"] = crossfilteredData.dimension(function (d) {
                    return d.StartYear;
                });


                dimensions["Finansii"] = crossfilteredData.dimension(function (d) {
                    return [+d.AmmountPayed, +d.AmmountToPay];
                });


                dimensions["Nationality"] = crossfilteredData.dimension(function (d) {
                    return d.Nationality;
                });


                dimensions["Count2"] = crossfilteredData.dimension(function (d) {
                    return 6;
                });

                console.log('End: Generating dimensions (' + (new Date().getTime() - t.getTime()) + ' ms)');
            }

            function generateGroups() {
                console.log('Start: Generating groups');
                var t = new Date();

                groups["StartYear"] = dimensions["StartYear"].group();

                groups["Sex"] = dimensions["Sex"].group();

                groups["SumCredits"] = dimensions["SumCredits"].group(function (d) {
                    return d - (d % 6);
                });

                groups["SumAverage"] = dimensions["SumAverage"].group();

                groups["LastSemester"] = dimensions["LastSemester"].group();

                groups["SemesterType"] = dimensions["SemesterType"].group();

                groups["TotalSemesters"] = dimensions["TotalSemesters"].group();

                groups["StudentStatus"] = dimensions["StudentStatus"].group();

                groups["Programme"] = dimensions["Programme"].group();

                groups["StudyCycle"] = dimensions["StudyCycle"].group();

                groups["Count"] = dimensions["Count"].group();

                dimensions["newDim"] = crossfilteredData.dimension(function (d) {
                    return ["Платени", "Dolzi"]
                });
                groups["AvgSumGroup"] = dimensions["newDim"].group().reduce(reduceAdd2, reduceRemove2, reduceInitial2);

                function reduceAdd2(p, v) {
                    if (v.AmmountPayed != null && v.AmmountPayed != 0) {
                        ++p.count;
                        p.total += v.AmmountToPay;
                        p.totalPayed += v.AmmountPayed;
                        p.totalTotal = p.total + p.totalPayed;
                    }
                    return p;
                }

                function reduceRemove2(p, v) {
                    if (v.AmmountPayed != null && v.AmmountPayed != 0) {
                        --p.count;
                        p.total -= v.AmmountToPay;
                        p.totalPayed -= v.AmmountPayed;
                        p.totalTotal = p.total + p.totalPayed;
                    }
                    return p;
                }


                function reduceInitial2() {
                    return { count: 0, total: 0, totalPayed: 0 };
                }


                groups["col1DimTotal"] = dimensions["Programme"].group().reduce(reduceAdd, reduceRemove, reduceInitial);


                function reduceAdd(p, v) {
                    if (v.SumAverage != null && v.SumAverage != 0) {
                        ++p.count;
                        p.total += v.SumAverage;
                    }
                    return p;
                }

                function reduceRemove(p, v) {
                    if (v.SumAverage != null && v.SumAverage != 0) {
                        --p.count;
                        p.total -= v.SumAverage;
                    }
                    return p;
                }

                function reduceInitial() {
                    return { count: 0, total: 0 };
                }

                groups["Nationality"] = dimensions["Nationality"].group();
                groups["StudentAge"] = dimensions["StudentAge"].group();
                groups["LastStudentQuota"] = dimensions["LastStudentQuota"].group();

                console.log('End: Generating groups (' + (new Date().getTime() - t.getTime()) + ' ms)');
            }


            charts['startYearChart'] = dc.barChart("#startYearChart")
                .width($("#startYearChart").width() + 30).height(chartHeight + 60)
                .dimension(dimensions["StartYear"])
                .group(dimensions["StartYear"].group(function (d) {
                    return d;
                }).reduceCount(function (d) {
                    return d;
                }))
                .margins(margins)
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                // .x(d3.scale.linear().domain([2011, 2015]))
                .x(d3.scaleBand().domain(groups["StartYear"].all().map(function (d) {
                    return d.key;
                })))
                .xUnits(dc.units.ordinal)
                .centerBar(false)
                .elasticY(true);

            charts['genderChart'] = dc.pieChart("#genderChart")
                .width(chartWidth + singleCircle)
                .height(chartHeight + singleCircle)
                .slicesCap(4)
                .innerRadius(innerRadius)
                .dimension(dimensions["Sex"])
                .group(groups["Sex"])
                .legend(dc.legend().legendText(function (d) {
                    return gender[d.name];
                }))
                .label(function (d) {
                    var v = (100 * d.value / dimensions.Sex.groupAll().value());
                    return gender[d.key] + ' ' + v.toFixed(0) + '%';
                })
                .title(function (d) {
                    return gender[d.key] + ': ' + d.value;
                })
                ;

            charts['sumCreditsChart'] = dc.lineChart("#sumCreditsChart")
                .width($("#startYearChart").width() + 30).height(chartHeight + 70)
                .dimension(dimensions["SumCredits"])
                .group(groups["SumCredits"])
                .margins(margins)
                .x(d3.scaleLinear().domain([0, 300]))
                .renderArea(true)
                .brushOn(true)
                .elasticY(true);

            charts['lastSemesterChart'] = dc.rowChart("#lastSemesterChart")
                .width(chartWidth).height(chartHeight * 3 - 40)
                .dimension(dimensions["LastSemester"])
                .group(groups["LastSemester"])
                .elasticX(true)
                .label(function (d) {
                    return d.key;
                })
                .ordering(function (d) {
                    return -d.key.substring(8, 11);
                });


            charts['semesterTypeChart'] = dc.pieChart("#semesterTypeChart")
                .width(chartWidth + singleCircle).height(chartHeight + singleCircle)
                //.innerRadius(100)
                .dimension(dimensions["SemesterType"])
                .group(groups["SemesterType"])
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .legend(dc.legend().legendText(function (d) {
                    return d.name;
                }))
                .label(function (d) {
                    var v = (100 * d.value / dimensions.SemesterType.groupAll().value());
                    return d.key + ' ' + v.toFixed(0) + '%';
                })
                .title(function (d) {
                    return d.key + ': ' + d.value;
                })
                ;


            charts['totalSemestersChart'] = dc.barChart("#totalSemestersChart")
                .width($("#totalSemestersChart").width() + 30).height(chartHeight + 60)
                .dimension(dimensions["TotalSemesters"])
                .group(groups["TotalSemesters"])
                .x(d3.scaleLinear().domain([0, 25]))
                .centerBar(false)
                .elasticX(false);

            charts['studentStatusChart'] = dc.pieChart("#studentStatusChart")
                .width(chartWidth + singleCircle).height(chartHeight + singleCircle)
                //.innerRadius(100)
                .dimension(dimensions["StudentStatus"])
                .group(groups["StudentStatus"])
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .label(function (d) {
                    var v = (100 * d.value / dimensions.StudentStatus.groupAll().value());
                    return d.key + ' ' + v.toFixed(0) + '%';
                })
                .legend(dc.legend())
                .title(function (d) {

                    return d.key + ': ' + d.value;
                });

            charts['statsChart'] = dc.numberDisplay("#statsChart")
                .valueAccessor(function (d) {

                    return d;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat1(d);
                })
                .group(dimensions["Count"].groupAll());

            charts['statsChartSide'] = dc.numberDisplay("#statsChartSide")
                .valueAccessor(function (d) {

                    return d;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat1(d);
                })
                .group(dimensions["Count"].groupAll());


            charts['statsChart2'] = dc.numberDisplay("#statsChart2")
                .valueAccessor(function (d) {
                    return d.value;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span> ",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat2(d);
                })
                .group(dimensions['Count2'].group().reduceSum(function (d) { return d.SumCredits; }));

            charts['statsChart2Side'] = dc.numberDisplay("#statsChart2Side")
                .valueAccessor(function (d) {
                    return d.value;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat2(d);
                })
                .group(dimensions['Count2'].group().reduceSum(function (d) { return d.SumCredits; }));


            var total = dimensions["Count"].groupAll();
            $("#totalCount").addClass('count');
            $("#totalCount").text(numberFormat1(total.value()) + '');
            $("#totalCountSide").text(numberFormat1(total.value()) + '');

            $("#totalCredits").addClass('count');
            $("#totalCredits").text(numberFormat2(dimensions['Count2'].group().reduceSum(function (d) { return d.SumCredits; }).top(1)[0].value) + '');
            $("#totalCreditsSide").text(numberFormat2(dimensions['Count2'].group().reduceSum(function (d) { return d.SumCredits; }).top(1)[0].value) + '');



            charts['programmeChart'] = dc.rowChart("#programmeChart")
                .width(chartWidth + 30).height(chartHeight * 3 - 40)
                .dimension(dimensions["Programme"])
                .group(groups["Programme"])
                .elasticX(true)
                .label(function (d) {
                    return d.key[0] + " Бр.:" + d.value;
                })
                .renderTitle(true)
                .title(function (p) {
                    return p.key[1] + " Број на студенти: " + p.value;
                })
                .ordering(function (d) {
                    return -d.value
                })
                .rowsCap(25)
                .gap(1);


            charts['studyCycleChart'] = dc.pieChart("#studyCycleChart")
                .width(chartWidth + singleCircle).height(chartHeight + singleCircle)

                .dimension(dimensions["StudyCycle"])
                .group(groups["StudyCycle"])
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .legend(dc.legend().legendText(function (d) {
                    return d.name;
                }))
                .label(function (d) {
                    var v = (100 * d.value / dimensions.StudyCycle.groupAll().value());
                    return d.key + ' ' + v.toFixed(0) + '%';
                })
                .title(function (d) {
                    return d.key + ': ' + d.value;
                });


            charts['averageChart'] = dc.pieChart("#averageChart")
                .width(chartWidth + singleCircle).height(chartHeight + singleCircle)
                .innerRadius(innerRadius)
                .dimension(dimensions["SumAverage"])
                .group(groups["SumAverage"])
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .legend(dc.legend().legendText(function (d) {
                    return gradeList[d.name];
                }))
                .label(function (d) {
                    return gradeList[d.key];
                })
                .title(function (d) {
                    return gradeList[d.key] + ': ' + d.value;
                });


            charts['financialPayedChart'] = dc.numberDisplay("#financialPayedChart")
                .valueAccessor(function (d) {
                    return d.value;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat2(d);
                })
                .group(dimensions['newDim'].group().reduceSum(function (d) { return d.AmmountPayed / 61.5; }));

            charts['financialToPayChart'] = dc.numberDisplay("#financialToPayChart")
                .valueAccessor(function (d) {
                    return d.value;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat2(d);
                })
                .group(dimensions['newDim'].group().reduceSum(function (d) {
                    return d.AmmountToPay / 61.5;
                }));

            charts['financialDifference'] = dc.numberDisplay("#financialDifference")
                .valueAccessor(function (d) {
                    return d.value;
                })
                .html({
                    one: "<span class=\"count\">%number</span>",
                    some: "<span class=\"count\">%number</span>",
                    none: "<span class=\"count\">0</span>"
                })
                .formatNumber(function (d) {
                    return numberFormat2(d);
                })
                .group(dimensions['newDim'].group().reduceSum(function (d) {
                    return (d.AmmountPayed - d.AmmountToPay) / 61.5;
                }));
            charts['NationalityChart'] = dc.pieChart("#NationalityChart")
                .width(chartWidth + singleCircle).height(chartHeight + singleCircle)
                .innerRadius(innerRadius)
                .dimension(dimensions["Nationality"])
                .group(groups["Nationality"])
                .colors(d3.scaleOrdinal(d3.schemeCategory20))
                .legend(dc.legend())
                .label(function (d) {
                    return d.key;
                })
                .title(function (d) {
                    return d.key + ': ' + d.value;
                });

            charts['avgByProgramme'] = dc.rowChart("#avgByProgramme")
                .width(chartWidth).height(chartHeight * 3 - 30)
                .dimension(dimensions["Programme"])
                .group(groups["col1DimTotal"])
                .elasticX(true)
                .othersGrouper(null)
                .label(function (d) {

                    return d.key[0] + " " + (d.value.total / d.value.count).toFixed(2);

                })
                .renderTitle(true)
                .title(function (p) {
                    return p.key[1] + "\nПросек " + (p.value.total / p.value.count).toFixed(2) + "\nБрој студенти: " + p.value.count;;
                })
                .ordering(function (d) {
                    return -d.value.count;
                })
                .rowsCap(25)
                .gap(1)
                .valueAccessor(function (p) { return p.value.count > 0 ? p.value.total / p.value.count : 0; });

            charts['StudentAgeChart'] = dc.lineChart("#StudentAgeChart")
                .width($("#StudentAgeChart").width() + 30).height(chartHeight + 45)
                .dimension(dimensions["StudentAge"])
                .group(groups["StudentAge"])
                .margins(margins)
                .x(d3.scaleLinear().domain([21, 50]))
                .renderArea(true)
                .brushOn(true)
                .elasticY(true);

            charts['StudentQuota'] = dc.pieChart("#StudentQuota")
                .width(chartWidth).height(chartHeight)
                .cx(chartWidth / 2 + 35)
                .innerRadius(innerRadius)
                .dimension(dimensions["LastStudentQuota"])
                .group(groups["LastStudentQuota"])
                .colors(d3.scaleOrdinal(d3.schemePaired))
                .legend(dc.legend())
                .label(function (d) {
                    var v = (100 * d.value / dimensions.LastStudentQuota.groupAll().value());
                    return d.key + ' ' + v.toFixed(0) + '%';
                })
                .title(function (d) {
                    var v = (100 * d.value / dimensions.LastStudentQuota.groupAll().value());
                    return d.key + ' ' + v.toFixed(0) + '%' + "\n" + d.key + ': ' + d.value;
                });
            dc.renderAll();
        }

        generateChartTitles();


        function generateChartTitles() {
            var chartTitles = ['', 'Година на запишување', 'Пол', 'Просек', 'Освоени кредити',
                'Последно запишан семестар', 'Тип на последен семестар',
                'Вкупно запишани семестри', 'Статус', '', 'Насока', 'Циклус на студии'];

            for (var i = 1; i <= chartTitles.length; i++) {
                $("#title" + i).text(chartTitles[i]);
            }
        }
    }
});

dc.redrawAll = function (group) {
    var charts = dc.chartRegistry.list(group);
    for (var i = 0; i < charts.length; ++i) {
        charts[i].redraw();
    }
    if (dc._renderlet !== null) {
        dc._renderlet(group);
    }
};
