//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinkiData
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditLogShort
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> EventDate { get; set; }
        public string TableName { get; set; }
        public string UserName { get; set; }
        public string Actions { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public string ChangedColumns { get; set; }
        public string OriginalData { get; set; }
    }
}
